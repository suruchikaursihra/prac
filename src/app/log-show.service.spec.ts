import { TestBed, inject } from '@angular/core/testing';

import { LogShowService } from './log-show.service';

describe('LogShowService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogShowService]
    });
  });

  it('should be created', inject([LogShowService], (service: LogShowService) => {
    expect(service).toBeTruthy();
  }));
});
