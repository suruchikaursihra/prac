import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { RouterModule, Routes, Router } from '@angular/router';
import { THROW_IF_NOT_FOUND } from '../../../node_modules/@angular/core/src/di/injector';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  title = 'reactive-forms';
  login: FormGroup;
  checkpass:Boolean;
  pass:string;
  pass1:string;

  constructor(private route: Router) {
    this.login = new FormGroup({
      Fname: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      Lname: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")]),
      email: new FormControl('', [Validators.required, Validators.email]),
      contact: new FormControl('', [Validators.required, Validators.pattern("^(([0-9]*)|(([0-9]*)))$")]),
      password: new FormControl('', [Validators.required]),
      cnfpassword: new FormControl('', [Validators.required]),
      eid: new FormControl('', [Validators.required, Validators.pattern("^(([0-9]*)|(([0-9]*)))$")]),
      gender: new FormControl('', [Validators.required, Validators.pattern("^[a-zA-Z]+$")])
    });
  }


  ngOnInit() {
    if(this.route.url==='/main')
{
    let value = JSON.parse(localStorage.getItem("val"));
    this.login.patchValue({
      Fname: value.Fname,
      Lname: value.Lname,
      contact: value.contact,
      gender: value.gender,
      eid: value.eid,
      email: value.email,
      password: value.password,
      cnfpassword: value.cnfpassword
    });
  }
  
}
  loc() {
    localStorage.setItem("val", JSON.stringify(this.login.value));
    this.route.navigate(['/table']);

  }

  check(){
    if(this.login.value.password!==this.login.value.cnfpassword)
    this.checkpass=false;
    else
    this.checkpass=true;
    console.log(this.checkpass);
    }

    showpass(){
      if(this.pass=="password")
      this.pass='text';
      else{
        this.pass="password"; 
      }
    }
      showpass1(){
        if(this.pass1=="password")
        this.pass1='text';
        else{
          this.pass1="password"; 
        }
    }
}

