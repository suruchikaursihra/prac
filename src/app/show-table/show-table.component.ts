import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-show-table',
  templateUrl: './show-table.component.html',
  styleUrls: ['./show-table.component.css']
})
export class ShowTableComponent implements OnInit {

  value:Object
  constructor(private route: Router) {
    this.value=JSON.parse(localStorage.getItem("val"));
   }


  ngOnInit() {
  }

  redirect(){
    this.route.navigate(['/main']);
  }

}
